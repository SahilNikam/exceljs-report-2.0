const express = require('express');
const path = require('path');
const multer  = require('multer');

const main = require('./src/main');

const app = express();

app.set("view engine", "ejs");
app.set("views", path.resolve("./views"));

app.use(express.urlencoded( { extended: false } ));

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        return cb(null, "./uploades");
    },
    filename: function (req, file, cb) {
        return cb(null, file.originalname )
    }
});

const upload = multer({ storage });

app.post('/upload',upload.fields([
    { name: 'CJsouth' }, 
    { name: 'weekly' } 
  ]) ,
  (req, res) => {
    console.log("file saved");
    main(req.body.date, req.files.CJsouth[0].originalname, req.files.weekly[0].originalname);
    return res.render('homepage');
});

app.get('/', (req, res) => {
    return res.render("homepage");
})

PORT = 3000;
app.listen(PORT,() => console.log("Listening the port", PORT));