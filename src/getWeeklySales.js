const ExcelJS = require('exceljs');
const path = require('path');

const controller = require('../src/storeRecords');
const map = controller.map;
const weeklySales = new ExcelJS.Workbook();


async function getWeeklySales(weekNumber, weeklyFilePath='weekly-1719346411030'){
    const filePath = path.join("C:\\Users\\pravi\\OneDrive\\Desktop\\Task\\uploades", weeklyFilePath);
    await  weeklySales.xlsx.readFile(filePath);
    console.log("Weekly file Readed Successfully")
    const worksheet = weeklySales.getWorksheet(`Week ${weekNumber}`);
    if(!worksheet){
        console.log(`Worksheet of Week ${weekNumber} Does not exist in weekly file`);
    }   
    const last = worksheet.actualRowCount;
    let row = 9;
    while(row<=last){
        const key = worksheet.getCell(`A${row}`).value;
        if(map.has(key)){
            map.get(key).push(worksheet.getCell(`C${row}`).value);
        }
        row++;
    }
    if(row != 9){
        console.log("Weekly records added successfully");
    }
}

module.exports = getWeeklySales;