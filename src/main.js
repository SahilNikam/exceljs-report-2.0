const path = require('path');

const addWorksheet = require('./addWorksheet');
const controller  = require('./storeRecords');
const getWeeklySales = require('./getWeeklySales');
const moment = require("moment");
const modifySheet = require('./modifySheet');

const ExcelJS = require('exceljs');

const map = controller.map;

let workbook = new ExcelJS.Workbook();
let weekNumber;

function getWeek(date){
    weekNumber = moment(date,"YYYY-MM-DD").isoWeek()-1;
    console.log("Current Week Number is "+weekNumber);
    return weekNumber;
}

async function run(date, CJsouthFilePath, weeklyFilePath){
    weekNumber = getWeek(date);
    await getSheet(weekNumber);
    await getWeeklySales(weekNumber, weeklyFilePath);
    await storeVoide(CJsouthFilePath);
    const sheet = await modifySheet(workbook, weekNumber);
    await saveSheet(sheet);
}

async function getSheet(weekNumber){
    await workbook.xlsx.readFile('./Target.xlsx')
    main = workbook.getWorksheet(`Week ${weekNumber}`);
    if(!main){
        console.log(`Worksheet of Week ${weekNumber} Does not exist in target sheet`);
        main = addWorksheet(workbook, weekNumber);
    }
    else{
        console.log("This Sheet is already exists in target sheet");
    }
    main = controller.saveRecords(workbook, main);
}

async function storeVoide(CJsouthFilePath='southExpient-1719346411344'){
    const filePath = path.join("C:\\Users\\pravi\\OneDrive\\Desktop\\Task\\uploades", CJsouthFilePath);
    await workbook.xlsx.readFile(filePath);
    const worksheet2 = workbook.getWorksheet(`Summary`);
    if(!worksheet2){
        console.log(`Worksheet of Summary Does not exist`);
        return;
    }
    const row = worksheet2.getRow(6);
    row.eachCell((cell, colNumber) => {
        const key = cell.value;
        if(map.has(key)){
            map.get(key).push(worksheet2.getCell(118,colNumber).value.result);
        } 
    });
}

async function saveSheet(sheet){
    if(!sheet) return;
    workbook.xlsx.writeFile('./Target.xlsx')
        .then(()=> console.log("Final Data saved successfully"))
        .catch(()=> console.log("Cannot read this file"));
}

module.exports = run;


