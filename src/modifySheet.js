const controller  = require('./storeRecords');
const map = controller.map;


async function modifySheet(workbook, weekNumber){
    workbook = await workbook.xlsx.readFile('./Target.xlsx'); // Read Target file sheet
    const sheet = workbook.getWorksheet(`Week ${weekNumber}`); // get sheet of that week
    let num = 2;
    let arr = [];

    map.forEach((value, key) => {
        if(value[3] && value[4]){
            value.push({formula: `ROUND((E${num}/D${num}%), 1)`, result: ((value[4].value*100)/value[3].value).toFixed(1) }); // make voids column
        }
        arr.push(value);
        num++;
    });

    const lastvalue = arr.pop(); // remove null in order to sort the the array

    arr.sort((a, b) => {
        const resultA = parseFloat(a[a.length - 1].result);
        const resultB = parseFloat(b[b.length - 1].result);
        return resultB - resultA ;
      });
    arr.push(lastvalue)

    const borderStyle = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
    };

    arr.forEach((row, rowIndex) => {
        if(row){
            const excelRow = sheet.addRow(row);
            row.forEach((value, colIndex) => {
                const cell = excelRow.getCell(colIndex + 1);
                cell.alignment = { vertical: 'middle', horizontal: 'center' };
                cell.border = borderStyle;
            });
        }
    });

    const totalSales = sheet.getCell('D55');
    totalSales.value = { formula: 'ROUND(SUM(D2:D54), 1)' };
    totalSales.border = borderStyle;
    totalSales.alignment = { vertical: 'middle', horizontal: 'center' };

    const totalVoide = sheet.getCell('E55');
    totalVoide.value = { formula: 'ROUND(SUM(E2:E54), 1)'};
    totalVoide.border = borderStyle;
    totalVoide.alignment = { vertical: 'middle', horizontal: 'center' };

    const totalPercent = sheet.getCell('F55');
    totalPercent.value = { formula : 'ROUND(E55/D55%, 1)'} ;
    totalPercent.border = borderStyle;
    totalPercent.alignment = { vertical: 'middle', horizontal: 'center' };

    if(sheet.actualRowCount >= 57){
        return; // if data is already resent do not append ahead of it.
    }
    return sheet;
}

module.exports = modifySheet;