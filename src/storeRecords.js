const ExcelJS = require('exceljs');

const workbook = new ExcelJS.Workbook();
const map = new Map();

function saveRecords(workbook, worksheet){
    newWorksheet = workbook.getWorksheet(`Week 24`);
    let num = 2;
    const last = newWorksheet.actualRowCount;
    while(num<=last){
        const key = newWorksheet.getCell(`B${num}`).value;
        map.set(key, [ newWorksheet.getCell(`A${num}`).value, key, newWorksheet.getCell(`C${num}`).value]);
        num++;
    }
    return worksheet;
}

module.exports = {
    saveRecords,
    map
};