async function addWorksheet(workbook, weekNumber){
    let worksheet = workbook.addWorksheet(`Week ${weekNumber}`);
    
    const borderStyle = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
    };

    worksheet.columns = [
        { header: 'Store', key: 'store', width: 15},
        { header: 'Store Id', key: 'storeId' },
        { header: 'DM', key: 'dm', width: 20 },
        { header: 'SALES', key: 'sales', width: 12 },
        { header: 'VOIDS', key: 'voids' },
        { header: '%', key: 'percentage' }
    ];
    const letters = ['A', 'B', 'C', 'D', 'E', 'F'];
    for (let col = 1; col <= 6; col++) {
        const cell = worksheet.getCell(`${letters[col-1]}1`);
        cell.style = {
            font: { bold: true },
            fill: { type: 'pattern', pattern: 'solid', fgColor: { argb: 'FFFF00' } },
            alignment: { horizontal: 'center' }
        };
        cell.border = borderStyle;
    }

    await workbook.xlsx.writeFile('./Target.xlsx')
        .then(()=> console.log("Data saved successfully"))
        .catch(()=> console.log("Cannot write this file"));
    return worksheet;
}

module.exports = addWorksheet;